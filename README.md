Monitoring Script
=========

Installs iwatch.
Configures detection of user changed, group changes and log directory deleted.

Requirements
-----------

Ansible installed on the remote host.

Role Variables
--------------
the directory in which the usergroup role will be installed <br>
usergroup_directory_name: usergroup <br>
<br>

the path which the usergroup role will be installed <br>
usergroup_role_directory: /var/www/monitor-usersgroupslogdir <br>
<br>
<br>
the directory which the playbook will be installed 
this playbook gets called by iwatch incase a user, group, log directory
is deleted. <br>
usergroup_playbook_directory: /var/www/ <br>

<br><br>
iwatch_xml: /var/www/iwatch.xml <br>



Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - monitoring-script
